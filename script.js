$(document).ready(function() {
    const imageInput = $('#imageInput');
    const imageDisplay = $('#imageDisplay');
    const uploadContainer = $('#uploadContainer');
    const imageIndexDisplay = $('#imageIndexDisplay');
    const backButton = $('#backButton');
    const forwardButton = $('#forwardButton');
    let currentImageIndex = 0;
    let images = [];
    let btnc = $('#btn');

    imageInput.on('change', handleImageUpload);
    $(document).on('keydown', handleKeyPress);
    backButton.on('click', handleBackButtonClick);
    forwardButton.on('click', handleForwardButtonClick);

    function handleImageUpload(event) {
        images = [];
        currentImageIndex = 0;
        imageDisplay.empty();
        imageIndexDisplay.empty();

        for (let i = 0; i < event.target.files.length; i++) {
            const file = event.target.files[i];
            const imageURL = URL.createObjectURL(file);
            const img = $('<img>').attr('src', imageURL);
            images.push(img);
        }

        if (images.length > 0) {
            displayImage(currentImageIndex);
            uploadContainer.hide();
            backButton.show();
            forwardButton.show();
        }
    }

    function displayImage(index) {
        imageDisplay.empty();
        imageIndexDisplay.empty();
        btnc.css('display', 'flex');

        if (index >= 0 && index < images.length) {
            imageDisplay.append(images[index]);
            images[index].css('display', 'block');

            const imageIndexText = `Image ${index + 1} of ${images.length}`;
            imageIndexDisplay.text(imageIndexText);
        }
    }

    function handleKeyPress(event) {
        if (event.code === 'ArrowRight') {
            handleForwardButtonClick();
        } else if (event.code === 'ArrowLeft') {
            handleBackButtonClick();
        }
    }

    function handleBackButtonClick() {
        currentImageIndex = (currentImageIndex - 1 + images.length) % images.length;
        displayImage(currentImageIndex);
    }

    function handleForwardButtonClick() {
        currentImageIndex = (currentImageIndex + 1) % images.length;
        displayImage(currentImageIndex);
    }

    var lastJsonVersion = null;

    function loadAndCompareJson() {
        
        $.ajax({
            url: "data.json",
            dataType: "json",
            cache: false,
            success: function (data) {
                    if (data.REF !== lastJsonVersion) {
                    
                    lastJsonVersion = data.REF;

                    if (data.REF === data.AA) {
                        handleBackButtonClick();
                        console.log("a")
                    } else if (data.REF === data.BB) {
                        handleForwardButtonClick();
                        console.log("b")
                    }
                }
            },
        });
    }

    setInterval(loadAndCompareJson, 10);
});
